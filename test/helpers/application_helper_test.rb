require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "TiwTiw is a microblogging site made just for you."
    assert_equal full_title("Help"), "Help | TiwTiw is a microblogging site made just for you."
    assert_equal full_title("Contact"), "Contact | TiwTiw is a microblogging site made just for you."
  end
end